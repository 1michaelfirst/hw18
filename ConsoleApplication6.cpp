﻿#include <iostream>
#include <string>
#include <algorithm>

class Player {
private:
    std::string name;
    int score = 0;
public:
    Player(std::string _name, int _score) : name(_name), score(_score)
    {}

    void SetName(std::string _name) {
        name = _name;
    }

    std::string GetName() const {
        return name;
    }

    void SetScore(int _score) {
        score = _score;
    }

    int GetScore() const {
        return score;
    }
};

int main()
{
    std::cout << "Players count: ";

    int playersCount;
    std::cin >> playersCount;

    Player* *players = new Player*[playersCount];

    for (int i = 0; i < playersCount; i++)
    {
        std::string name;
        int score;

        std::cout << "Player " << (i + 1) << " name: ";
        std::cin.ignore();
        std::getline(std::cin, name);

        std::cout << "Player " << (i + 1) << " score: ";
        std::cin >> score;

        auto player = new Player{ name, score };

        players[i] = player;
    }

    std::sort(players, players + playersCount, [](const auto& l, const auto& r) { return l->GetScore() > r->GetScore(); });

    for (int i = 0; i < playersCount; i++)
    {
        const auto player = players[i];
        std::cout << "Player " << (i + 1) << ": " << player->GetName() << ", score " << player->GetScore() << std::endl;
    }


    for (int i = 0; i < playersCount; i++)
    {
        delete players[i];
    }


    delete[] players;
}
